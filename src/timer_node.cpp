#include <timer_display_pkg/timer_node.h>

// TODO : 
// * Doit être un node ROS
//      - main nécess
//      - classe ac init + loop
// * Importer ce qui faut d'OpenCV
// * Mettre timer au milieu d'image
// * Ajouter stop / start timer. Tester.

namespace timerManagement
{

    timerNode::timerNode(ros::NodeHandle & nh)
    {
        std::string nodeName = ros::this_node::getName();
        nodeName += "/";

        nh_ = nh;


        double node_rate = 100;
        rosWrap::getRosParam(nodeName+"node_rate", node_rate,1);
        double node_period_ms = (1.0/node_rate)*1000;

        int size_x;
        int size_y;
        rosWrap::getRosParam(nodeName+"size_x", size_x);
        rosWrap::getRosParam(nodeName+"size_y", size_y);

        cv::namedWindow("Timer",cv::WINDOW_NORMAL);
        getTimerTrig = nh_.subscribe("/start_timer",1, &timerNode::manageTimerTrig, this);
        sTServer = nh_.advertiseService("/stop_timer",&timerNode::stopTrig,this);    
        pCTServer = nh_.advertiseService("/pause_continue_timer",&timerNode::pauseContinueTrig,this);    

        pubStopTime = nh_.advertise<std_msgs::Float64>("/timer_infos",1000);

        pubTimeLeft = nh_.advertise<std_msgs::Int32>("/time_left_timer",1000);    

        std::vector<int> timPos;
        timPos.push_back(size_x/2);
        timPos.push_back( (size_y)/2);        
        // timPos.push_back(0);
        // timPos.push_back( 0); 
        ros::Rate rate(node_rate);

        pU.setTimer(-1,-1,timPos);
        // pU.timeDisplayed = "Wait for first timer";

        pU.timeDisplayed = "Pas de Timer";
        int lastTL = -1;

        while (ros::ok())
        {
            if (timePub)
            {
                std_msgs::Float64 msg;
                msg.data = valTime;
                pubStopTime.publish(msg);
                timePub = false;
            }
  
            cv::Mat img = cv::Mat::zeros(cv::Size(size_x,size_y),CV_8UC3);
            double curTime = ros::Time::now().toSec();
            //std::cout << "timSet" << std::endl;
            bool red = false;

            if (pU.timeLeft > 0)
            {
                if (pU.timeLeft < curDur/4)
                {
                    red = true;
                }

                int val = int(pU.timeLeft);

                if (val !=  lastTL)
                {
                    std_msgs::Int32 msg;
                    lastTL = val;
                    msg.data = val;
                    pubTimeLeft.publish(msg);
                }

                if (!pU.isTimerActive)
                {

                    pU.timeDisplayed = "Pas de Timer";
                 
                }


            }
            else if (pU.isTimerActive)
            {

                //std::cout << "TIMEOUT" << std::endl;

                // for directly stopping timer
                // double curTime = ros::Time::now().toSec();
                // std::vector<double> result = pU.stopTimer(curTime);                
                valTime = pU.timerDuration;

                timePub = true;
                pU.timeDisplayed = "TIMEOUT";
                pU.isTimerActive = false;
            }

            

            pU.updateTimer(img,curTime, red);

            cv::imshow("Timer", img);
            cv::waitKey(node_period_ms); // Wait for a keystroke in the window

            rate.sleep();
            ros::spinOnce();
        }
    }

    bool timerNode::stopTrig(msg_srv_action_gestion::GetFloat::Request &req,
    msg_srv_action_gestion::GetFloat::Response &res)
    {
        double curTime = ros::Time::now().toSec();
        std::vector<double> result = pU.stopTimer(curTime);
        for (int i = 0; i < result.size(); i++)
        {
            std::cout << result[i] << std::endl;
        }
        res.data.data =(result[1] - result[0]);
        res.success = true;

        valTime = res.data.data;
        timePub = true;

        return(true);
    }



    void timerNode::manageTimerTrig(const std_msgs::Float64 & msg)
    {
        //std::cout << "Got timer trigger!" << std::endl;  
        if (msg.data >= 0) 
        {
            pU.setTimer(msg.data);
            curDur = msg.data;
        }
        double curTime = ros::Time::now().toSec();
        //std::cout << "Start time : " << curTime << std::endl;
        pU.activateTimer(curTime);
    }

    bool timerNode::pauseContinueTrig(std_srvs::SetBool::Request &req,
    std_srvs::SetBool::Response &res)
    {

        pU.pauseTimer(req.data, ros::Time::now().toSec());

        res.success = true;

        return(true);
    }

};

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "fov_manager");
  ros::NodeHandle nh;

   timerManagement::timerNode tM(nh);
  return 0;
}