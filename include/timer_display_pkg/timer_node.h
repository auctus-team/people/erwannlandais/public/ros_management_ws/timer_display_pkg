#include <ros/ros.h>

#include <tf2_ros/transform_listener.h>
#include <eigen3/Eigen/Core>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Pose.h>
#include <eigen3/Eigen/Geometry>

#include <typeinfo>

#include "opencv_pinhole_management/pinhole_utils.h"

#include <msg_srv_action_gestion/SetCamInfo.h>
#include <msg_srv_action_gestion/SetString.h>
#include <msg_srv_action_gestion/SetStringWR.h>
#include <msg_srv_action_gestion/Float64MultiArrayStamped.h>
#include <msg_srv_action_gestion/GetFloat.h>


#include "stdio.h"

#include "std_msgs/Int32.h"
#include "std_msgs/Float64.h"
#include "std_srvs/SetBool.h"

#include "ros_wrap/rosparam_getter.h"

#include "opencv_pinhole_management/pinhole_utils.h"


namespace timerManagement
{
    class timerNode
    {
        public:
            timerNode(ros::NodeHandle & nh);

            pinholeUtils pU;

            ros::ServiceServer sTServer;
            ros::ServiceServer pCTServer;

            ros::Publisher pubStopTime;
            bool timePub=  false;
            double valTime = -1;

            ros::NodeHandle nh_;

            double curDur = -1;


            ros::Subscriber getTimerTrig;

            ros::Publisher pubTimeLeft;

            bool stopTrig(msg_srv_action_gestion::GetFloat::Request &req,
    msg_srv_action_gestion::GetFloat::Response &res);

            void manageTimerTrig(const std_msgs::Float64 & msg);
   
    bool pauseContinueTrig(std_srvs::SetBool::Request &req,
    std_srvs::SetBool::Response &res);
    };

}